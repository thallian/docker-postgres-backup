FROM registry.gitlab.com/thallian/docker-confd-env:master

ENV PG_PORT 5432
ENV PG_EXTRA_OPTS '-Z9'
ENV BACKUP_DIR /var/lib/postgresql/backup
ENV BACKUP_KEEP_DAYS 7
ENV BACKUP_KEEP_WEEKS 4
ENV BACKUP_KEEP_MONTHS 6
ENV CRON_SCHEDULE */15  *  *  *  *

RUN apk add --no-cache postgresql

RUN mkdir -p $BACKUP_DIR
RUN chown -R postgres:postgres /var/lib/postgresql

ADD /rootfs /

VOLUME $BACKUP_DIR
