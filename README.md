Periodic backups of postgre databases.

# Volumes
- `/var/lib/postgresql/backup`

# Environment Variables
## CRON_SCHEDULE
- default: */15  *  *  *  *

When to run the backup script, in crontab format.

## PG_PORT
- default: 5432

Port on which to connect to the postgre host.

## PG_EXTRA_OPTS
- default: -Z9

Additional options for [pg_dump](https://www.postgresql.org/docs/9.6/static/app-pgdump.html).

## PG_HOST

The postgre host to connect to.

## PG_USER

User for the postgre connection.

## PG_PASSWORD

Password for the postgre connection.

## PG_DB

The database to backup.

## BACKUP_KEEP_DAYS
- default: 7

How many daily backups to keep.

## BACKUP_KEEP_WEEKS
- default: 4

How many weekly backups to keep.

## BACKUP_KEEP_MONTHS
- default: 6

How many monthly backups to keep.

# Capabilities
- CHOWN
- DAC_OVERRIDE
- FOWNER
- NET_BIND_SERVICE
- SETGID
- SETUID
